<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function concedra_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate site information
  $form['site_information']['site_name']['#default_value'] = 'Concedra GmbH Demo';
  $form['site_information']['site_mail']['#default_value'] = 'sysop@concedra.de';

  // Pre-populate admin account
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'root@concedra.de';

  // Pre-populate regional settings
  $form['regional_settings']['site_default_country']['#default_value'] = 'DE';
}
